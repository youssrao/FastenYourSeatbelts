#include <stdio.h>

int main(int argc, char **argv){
	printf("Software downloaden...\n");
	system("apt-get update");
	system("apt-get install libusb-dev");
	system("apt-get install motion");
	system("apt-get install libmysqlclient-dev");

	system("wget http://www.phidgets.com/downloads/libraries/libphidget.tar.gz");
	system("wget https://oege.ie.hva.nl/~vriesmg001/BuildingManager/BuildingManager.c");
	system("wget https://oege.ie.hva.nl/~vriesmg001/BuildingManager/curlpush.sh");
	system("wget https://oege.ie.hva.nl/~vriesmg001/BuildingManager/motion.conf");

	system("tar xvf li*");
	system("cd li*");
	system("./configure");
	system("make");
	system("make install");
	system("cd ../");
	system("gcc -o BuildingManager $(mysql_config --cflags) BuildingManager.c  -lphidget21 $(mysql_config --libs)")
	printf("Setup complete\n");
	printf("Druk op een knop om door te gaan...");
	getchar();
	return 0;
}
