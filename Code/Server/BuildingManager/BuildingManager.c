#include <my_global.h>
#include <mysql.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <phidget21.h>
#include <string.h>

#define SECOND     1000000
#define INPUTDELAY (SECOND / 10)
#define SQLDELAY   (SECOND * 0.5)
#define TEMPDELAY  60
#define CONSLEEP   (SECOND / 100)
#define SLOTDELAY  (SECOND * 2)

/*
* ifKit = InterFaceKit used by leds and phidgets.
* rfidKit = Kit used by the rfid Scanner.
*/
CPhidgetInterfaceKitHandle ifKit = 0;
CPhidgetRFIDHandle rfidKit = 0;

/*
* Variables used to connect to server.
* server = location of the database.
* database = name of the database.
* username = username used to log in.
* password = password used to log in.
* connection = used to "talk" to the database.

* conUsed = Used to lock the connection
* this makes sure the threads dont push a query
* at the same time.

* try = used to stop the pushAlert function.
* setTry = used to stop the setCurrentState function.
*/
char *server = "oege.ie.hva.nl";
char *database = "zvriesmg001";
char *username = "vriesmg001";
char *password = "iMKXdn0rHt/gsn";
MYSQL *connection = 0;

int conUsed = 0;
int try = 0;
int setTry = 0;

/*
* Running variables: 0 = off 1 = on
* tempRun = Thermo thread on or off.
* inputRun = Input thread on or off.
* sqlRun = SQL thread on or off.
* rfidRun = RFID thread on or off.
* alarmRun = Alarm on or off.
*/
int tempRun = 0;
int inputRun = 0;
int sqlRun = 0;
int rfidRun = 0;
int alarmRun = 0;

/*
* knop  = The status of the alarm button.
* light = Lights on(1) or off(0).
* lock  = Lock open(0) or closed(1).
*/
int knop   = 0;
int lights = 0;
int lock   = 0;
/*
* currentTemp = Degrees in Celcius of the room.
* tempGoal    = Which temperature does the user want.
* heater = Heater on(1) or off(0).
* airco = Airco on(1) or off(0).
* mode   = Listen to database value(0) or determine itself(1).
* tempAlert = Did the system already push a warning?
* oldDate = The date of the last temperature check.
* yesterDate = The date yesterday.
*/
int currentTemp = 0;
int tempGoal    = 0;
int heater      = 0;
int airco       = 0;
int mode        = 0;
int tempAlert   = 0;
char oldDate[20] = "";
char yesterDate[20] = "";

/*
* temper = Thread used to messure and control temperature.
* input  = Thread used to check and handle buttons.
* sql    = Thread used to synchronize and handle database values.
* alarmThread = Thread used to blink alarm.
* doorThread = Thread used to open door.
*/
pthread_t temper;
pthread_t input;
pthread_t sql;
pthread_t alarmThread;
pthread_t doorThread;


//Function used to send message to the server.
//Takes the message and the invoker as char* arguments.
int pushAlert(char *alert, char *invoker){

  printf("Alert : %s\n", alert);
  /*
  * Query / error, used to store created strings.
  * datestr / timestr, used to store date and time.
  */
  char query[300];
  char error[300];
  char datestr[80];
  char timestr[80];

  //Initialize variables needed to gather time and date.
  time_t time_raw;
  struct tm *info;

  //Gather time and date.
  time(&time_raw);
  info = localtime(&time_raw);

  //Format and store into string.
  strftime(datestr , 80, "%d/%m/%Y", info);
  strftime(timestr, 80, "%H:%M:%S", info);

  //Check if theres a difference
  //between the new date and the last gathered date.
  if(strcmp(oldDate, datestr) != 0){
    //Check if the date of yesterday is not yet gathered.
    //If so, populate it.
    if((!strcmp(yesterDate, ""))){
      strcpy(yesterDate, newDate);
      printf("New date %s\n", yesterDate);
    }
    //If not, paste in the old date.
    else{
      strcpy(yesterDate, oldDate);
      printf("New data %s\n", datestr);
    }
  }
  //The newly gathered date will now be used in the next check as the old date.
  strcpy(oldDate, datestr);

  //Create the query.
  sprintf(query, "INSERT INTO %s.Meldingen (Date,Time,InvokedBy,Message) VALUES(\'%s\', \'%s\', \'%s\', \'%s\')", database, datestr, timestr, invoker, alert);

  //Wait for the connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }

  //Lock the connection and execute the query.
  //If it fails it will return 1. If succes it will return 0.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    //Try 5 times.
    if(try < 5){
      try++;
      pushAlert(error, "System");
    }
    //Kill the process if it tried 5 times.
    else{
      exit(1);
    }
  }
  //Reset and Return.
  conUsed = 0;
  try = 0;
  return 0;
}

//Function used to send temperature to the server.
//Takes the current temperature as an Integer argument.
int pushTemperature(int temperature){
  /*
  * Query / error, used to store created strings.
  * datestr / timestr, used to store date and time.
  */
  char query[400];
  char error[400];
  char datestr[30];
  char timestr[30];

  //Initialize variables.
  time_t time_raw;
  struct tm *info;

  //Gather and store data.
  time(&time_raw);
  info = localtime(&time_raw);
  strftime(datestr, 30, "%d/%m/%Y", info);
  strftime(timestr, 30, "%H:%M:%S", info);

  //Create query.
  sprintf(query, "INSERT INTO %s.Temperatuur (Datum,Tijd,Temperatuur, GewensteTemp) VALUES (\"%s\", \"%s\", %d, %d)", database, datestr, timestr, currentTemp, tempGoal);

  //Wait for the connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }

  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    exit(1);
  }
  //Reset and Return.
  conUsed = 0;
  return 0;
}


//Function used to change sensor data.
//Takes the designated table, column, sensor and value as arguments.
int setCurrentState(char *table, char *column, char *sensor, int value){
  printf("Setting %s for %s to %d\n");
  //Create variables.
  char query[200];
  char error[200];

  //Create query.
  sprintf(query, "UPDATE %s.%s SET %s = %d WHERE SNAAM = \"%s\" ", database, table, column, value, sensor);
  while(conUsed){
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    exit(1);
  }
  conUsed = 0;

  //After a second check if the sensor data updated.
  usleep(SECOND);
  if(getCurrentState(table, column, sensor) != value){
    //If it failed try again 2 times.
    if(setTry < 2){
      setCurrentState(table, column, sensor, value);
      setTry += 1;
    }
    else{
      sprintf(error, "%s van %s is niet veranderd naar %d door de volgende query %s", column, sensor, value, query);
      pushAlert(error, "System");
      exit(1);
    }
  }
  setTry = 0;
  return 0;
}

//Function will give the last person without a tag, a tag.
//Takes tag as char* as argument.
int newTag( char *tag){

  //Initialize variables.
  char query[200];
  char error[200];
  char *flush;

  MYSQL_RES *result;
  MYSQL_ROW row;

  //Create query.
  sprintf(query, "SELECT ID FROM %s.Personeel WHERE RFID IS NULL ORDER BY ID DESC LIMIT 1", database);

  //Wait for connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    return 0;
  }
  conUsed = 0;

  //Fetch gathered data from connection.
  result = mysql_store_result(connection);

  //Check if the reult is empty.
  if(!(row = mysql_fetch_row(result))){
    printf("Geen row\n");
    return 0;
  }
  //Get person ID.
  int id  = strtol(row[0], &flush, 10);

  //Create query
  sprintf(query, "UPDATE %s.Personeel SET RFID = \"%s\" WHERE ID = %d ", database, tag, id);
  while (conUsed) {
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    return 0;
  }

  //Reset and return.
  conUsed = 0;
  return 1;
}

/*
 * Function to check if the tag is registered and to who.
 * storage = the address of the char* to store the name into.
 * rfidval = the tag as char*
*/
int checkTag(char **storage, char *rfidval){
  //Initialize variables.
  MYSQL_RES *result;
  MYSQL_ROW row;

  char query[200];
  char error[200];
  char voornaam[100];
  char achternaam[100];
  char reply[100];

  //Create query.
  sprintf(query, "SELECT VOORNAAM, ACHTERNAAM FROM Personeel WHERE RFID = \"%s\"", rfidval);

  //Wait for connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    exit(1);
  }
  conUsed = 0;
  //Fetch and check gathered data.
  result = mysql_store_result(connection);

  if(result == NULL){
    sprintf(error, "Pas %s is niet geregistreerd", rfidval);
    pushAlert(error, "Handler");
    //If theres no tag registered the name should be empty.
    //The char* at the location given in the arguments should be empty.
    *storage = "";
    return 0;
  }

  if(!(row = mysql_fetch_row(result))){
    printf("Pas %s is niet geregistreerd\n", rfidval);
    //If theres no tag registered the name should be empty.
    //The char* at the location given in the arguments should be empty.
    *storage = "";
    return 0;
  }

  //If there is someone registered to that tag.
  //Store his / her first name in reply.
  sprintf(reply, "%s %s", row[0], row[1]);
  //The char* at the location given in the arguments should be the
  //same as reply.
  *storage = reply;
  //Return 1 meaning the tag is registered.
  return 1;
}

//Function used to gather the average wanted temperature at the given date.
int getAvgTemp(char *date){
  //Initialize variables.
  MYSQL_RES *result;
  MYSQL_ROW row;

  char query[200];
  char error[200];
  char *flush;
  int avg = 0;

  //Create query.
  sprintf(query, "SELECT AVG(GewensteTemp) FROM %s.Temperatuur WHERE Datum = \"%s\"", database, date);

  //Wait for connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    exit(1);
  }

  conUsed = 0;
  //Gather and check returned data.
  result = mysql_store_result(connection);

  if(result == NULL){
    sprintf(error, "Geen temperatuur geschiedenis beschikbaar voor %s", yesterDate);
    pushAlert(error, "Handler");
    return 18;
  }
  if(!(row = mysql_fetch_row(result))){
    sprintf(error, "Geen temperatuur geschiedenis beschikbaar voor %s", yesterDate);
    pushAlert(error, "Handler");
    return 18;
  }
  avg = strtol(row[0], &flush, 10);

  return avg;
}

//Fucntion used to get the data stored at the given record.
//Takes table, column and sensor as char* variables to determine what to fetch.
int getCurrentState(char *table, char *column, char *sensor){
  //Initialize variable.
  MYSQL_RES *result;
  MYSQL_ROW row;

  char query[200];
  char error[200];
  char *flush;

  //Create query.
  sprintf(query, "SELECT %s FROM %s.%s WHERE SNAAM = \"%s\"", column, database, table, sensor);
  //Wait for connection to be free.
  while(conUsed){
    usleep(CONSLEEP);
  }
  //Lock and execute.
  conUsed = 1;
  if(mysql_query(connection, query)){
    conUsed = 0;
    sprintf(error, "Kon de volgende query niet uitvoeren: %s met de volgende foutmelding: %s", query, mysql_error(connection));
    pushAlert(error, "System");
    exit(1);
  }
  conUsed = 0;
  //Gather and check the returned data.
  result = mysql_store_result(connection);

  if(result == NULL){
    sprintf(error, "Kon geen waarde vinden voor : %s", query);
    pushAlert(error, "System");
  }
  row = mysql_fetch_row(result);
  int state = strtol(row[0], &flush, 10);
  return state;
}

//Function used to "Handle" the temperature sensor.
//Will be called in a thread.
void *tempHandler(){
  //Initialize variables.
  tempRun = 1;
  pushAlert("Temperature thread gestart", "Handler");
  int value = 0;
  int timer = 0;
  //While the thread should be running... RUN..
  //tempRun will be set to 2 if the program wants to stop.
  //Reason for 2 is : 0 would mean it has already been stopped.
  while (tempRun == 1) {
    //Reset timer.
    timer = 0;
    //Gatherd sensor data.
    CPhidgetInterfaceKit_getSensorValue(ifKit, 0, &value);
    //Calculate temperature.
    value = ((value * 0.2222) - 61.1111);
    printf("Temperature: %d\n", value);

    //Check if the temperature is too high.
    //If so : take apropiate action/
    if((value > 60) && (value < 100) && (tempAlert == 0)){
      pushAlert("Temperatuur is te hoog!!", "System");
      tempAlert = 1;
    }
    if(value >= 100){
      printf("ALARM\n");
      setCurrentState("Sensoren", "VALUE", "Noodknop", 1);
      pushAlert("ALARM: TEMPERATUUR IS TE HOOG!!", "System");
    }
    if(value != -61){
      //Reset warning alert.
      tempAlert = 0;
      //Set global variable to the newly gathered temperature.
      //The store it in the database.
      currentTemp = value;
      pushTemperature(currentTemp);
    }
    //Turn on / off the heater and airco.
    //They should not be on at the same time.
    CPhidgetInterfaceKit_setOutputState(ifKit, 2, heater);
    CPhidgetInterfaceKit_setOutputState(ifKit, 3, airco);
    //If the program should decide its own temperature goal.
    if(mode){
      tempGoal = getAvgTemp(yesterDate);
    }
    //Wait for 1 minute then continue unless the thread wants to stop.
    while((timer < TEMPDELAY) && (tempRun == 1)){
      usleep(SECOND);
      timer++;
    }
  }
  //Signal that all operations have been stopped.
  tempRun = 0;
  pushAlert("Temperature Thread gestopt", "Handler");
  return NULL;
}

//Function used to "handle" the alarm.
void *alarmHandler(){
  printf("Alarm thread is gestart\n");
  //Blink the alarm light.
  while(alarmRun){
    CPhidgetInterfaceKit_setOutputState(ifKit, 6, 1);
    usleep(SECOND);
    CPhidgetInterfaceKit_setOutputState(ifKit, 6, 0);
    usleep(SECOND);
  }
  pushAlert("Alarm Thread gestopt", "Handler");
}

//Function used to handle buttons on the board.
void *iHandler(){
  printf("IHandler staat aan\n");
  pushAlert("InputHandler staat aan", "Handler");
  //Initialize variables.
  inputRun = 1;
  int state = 9999;
  int oldState = 9999;
  //While the thread should be running... RUN..
  //inputRun will be set to 2 if the program wants to stop.
  //Reason for 2 is : 0 would mean it has already been stopped.
  while(inputRun == 1){
    //Get the state of the alarm button: 0 = off, 1 = on.
    CPhidgetInterfaceKit_getInputState(ifKit, 1, &state);

    //If the button has been pressed for the first time
    if(state && !knop){
      //Set the alarm button as on.
      setCurrentState("Sensoren", "VALUE", "Noodknop", state);
      //Send warning message.
      pushAlert("ALARM KNOP IS INGEDRUKT", "Handler");
    }
    //Get the state of the light switch.
    CPhidgetInterfaceKit_getInputState(ifKit, 2, &state);

    //If the state of the switch changed.
    if(oldState != state){
      //Update the state.
      setCurrentState("Sensoren", "VALUE", "Licht", state);
      oldState = state;
    }
    //Sleep for a given delay stated above in the #define.
    usleep(INPUTDELAY);
  }
  //Signal that all actions have been stopped.
  inputRun = 0;
  pushAlert("Input Thread gestopt", "Handler");
  return NULL;
}

//Function used to gathered and process the data in the database.
void *sqlHandler(){
  //Initialize variables.
  int tempStorage = 0;
  sqlRun = 1;
  pushAlert("SQLThread staat aan", "Handler");
  //While the thread should be running... RUN..
  //sqlRun will be set to 2 if the program wants to stop.
  //Reason for 2 is : 0 would mean it has already been stopped.
  while(sqlRun == 1){
    //Gahther and process the database data for every sensor.
    tempStorage = getCurrentState("Sensoren", "VALUE", "RPIGlobal");
    if(tempStorage == 0){
      setCurrentState("Sensoren", "VALUE", "RPIGlobal", 1);
      printf("Replied to check\n");
    }

    tempStorage = getCurrentState("Sensoren", "VALUE", "RFID");
    if(tempStorage != rfidRun){
      printf("RFID veranderd naar %d\n", tempStorage);
      rfidRun = tempStorage;
    }

    tempStorage = getCurrentState("Sensoren", "VALUE", "Licht");
    if(tempStorage != lights){
      printf("Licht veranderd naar %d\n", tempStorage);
      lights = tempStorage;
      CPhidgetInterfaceKit_setOutputState(ifKit, 1, lights);
      if(lights){
        pushAlert("Licht staat aan", "Handler");
      }
      else{
        pushAlert("Licht staat uit", "Handler");
      }
    }

    tempStorage = getCurrentState("Sensoren", "VALUE", "Thermo");
    if((tempStorage == 0 && tempRun)){
      tempRun = 2;
    }
    else if(((tempStorage == 1) || ( tempStorage == 2)) && mode != (tempStorage - 1)){
      printf("Temperatuur modus veranderd naar %d\n", tempStorage);
      mode = tempStorage - 1;
      //If the temper thread is not yet running. Start it.
      if(tempRun == 0){
        tempRun = 1;
        pthread_create(&temper, NULL, tempHandler, NULL);
      }
    }

    tempStorage = getCurrentState("Sensoren", "ARGUMENTS", "Thermo");
    if(tempStorage != tempGoal && (mode == 0)){
      tempGoal = tempStorage;
      printf("tempGoal changed to %d\n", tempGoal);
      if(currentTemp < tempGoal){
        pushAlert("Verwarming aanzetten", "Handler");
        airco = 0;
        heater = 1;
      }
      if(currentTemp > tempGoal){
        pushAlert("Airco aanzetten", "Handler");

        heater = 0;
        airco = 1;
      }
      CPhidgetInterfaceKit_setOutputState(ifKit, 2, heater);
      CPhidgetInterfaceKit_setOutputState(ifKit, 3, airco);
    }

    tempStorage = getCurrentState("Sensoren", "VALUE", "Slot");
    if(tempStorage != lock){
      lock = tempStorage;
      printf("Deur veranderd naar %d\n", tempStorage);
      if(lock){
        CPhidgetInterfaceKit_setOutputState(ifKit, 4, 1);
        usleep(SECOND);
        CPhidgetInterfaceKit_setOutputState(ifKit, 4, 0);
        pushAlert("Deur open", "Handler");
      }
      else{
        CPhidgetInterfaceKit_setOutputState(ifKit, 5, 1);
        usleep(SECOND);
        CPhidgetInterfaceKit_setOutputState(ifKit, 5, 0);
        pushAlert("Deur dicht", "Handler");
      }
    }

    tempStorage = getCurrentState("Sensoren", "VALUE", "Noodknop");
    if(tempStorage != knop){
      printf("Noodknop veranderd naar %d\n", tempStorage);
      knop = tempStorage;
      alarmRun = tempStorage;
      pthread_create(&alarmThread, NULL, alarmHandler, NULL);
    }
    usleep(SQLDELAY);
  }
  //Notify that the thread has been stopped.
  pushAlert("SQL Thread is gestopt", "Handler");
  sqlRun = 0;
  return NULL;

}

//Function that will be called when a tag has been lost.
//TODO : Fix error : tag sometimes never gets lost.
//This error happens when registerd tag has been read.
int CCONV tagLostHandler(CPhidgetRFIDHandle kit, void *usrptr, char *TagVal, CPhidgetRFID_Protocol proto){
	printf("Tag Lost: %s\n", TagVal);
	return 0;
}

//Function that will be called when the door needs to be opened.
void* openDoor(){
  setCurrentState("Sensoren", "VALUE", "Slot", 1);
  usleep(SLOTDELAY);
  setCurrentState("Sensoren", "VALUE", "Slot", 0);
  return 0;
}

//Function that will be called when a tag has been read.
//TODO : Fix error : tag sometimes never gets lost.
//This error happens when registerd tag has been read.
int CCONV tagHandler(CPhidgetRFIDHandle kit, void *usrptr, char *tagVal, CPhidgetRFID_Protocol protocol){
  printf("Called tagHandler\n");
  char *message;
  char *name;
  if(rfidRun > 0){
    int reply = checkTag(&name, tagVal);
    if(reply){
      sprintf(message, "Deur openen voor: %s met de pas: %s", name, tagVal);
      pushAlert(message, "Handler");
      pthread_create(&doorThread, NULL, openDoor, NULL);
    }
    else{
      //If the tag is not registered, but the rfid is in register mode.
      if(rfidRun == 2){ // <-- 2 = register mode, 1 is read mode.
        int reply = newTag(tagVal);
        if(reply){
          reply = 0;
          //Check if the tag is registered.
          //If so, store the name on the location of name.
          reply = checkTag(&name, tagVal);
          sprintf(message, "%s is nu geregistreerd op de pas %s", name, tagVal);
          pushAlert(message, "Handler");
          if(reply){
            reply = 0;
            sprintf(message, "Deur openen voor: %s met de pas: %s", name, tagVal);
            pushAlert(message, "Handler");
            pthread_create(&doorThread, NULL, openDoor, NULL);
          }
          else{
            sprintf(message, "Kon de identiteit van %s niet ophalen", tagVal);
            pushAlert(message, "Handler");
          }
        }
        else{
          sprintf(message, "Kon de gebruiker %s niet registreren", tagVal);
          pushAlert(message, "Handler");
        }
      }
      else{
        sprintf(message, "Toegang geweigerd voor %s", tagVal);
        pushAlert(message, "Handler");
      }
    }
  }
  else{
    printf("RFID staat uit\n");
  }
  printf("Ended\n");
  return 0;
}

//Function that is called to start up all wanted threads.
//If not yet running.
int setupThreads(){
  pushAlert("Threads opstarten", "Handler");
  printf("SQL : %d\n", sqlRun);
  printf("Input : %d\n", inputRun);
  printf("Temperature : %d\n", tempRun);
  printf("Alarm : %d\n", alarmRun);
  if(!sqlRun){
    if(pthread_create(&sql, NULL, sqlHandler, NULL)){
      pushAlert("Kon de volgende thread niet starten : sql", "System");
      pushAlert("Shutting down", "System");
      exit(1);
    }
    else{
      printf("SQLThread created\n");
    }
  }
  if(!inputRun){
    inputRun = 1;
    if(pthread_create(&input, NULL, iHandler, NULL)){
      pushAlert("Kon de volgende thread niet starten : input", "System");
      exit(1);
    }
    else{
      printf("InputThread created\n");
    }
  }
  if(!tempRun){
    tempRun = 1;
    if(pthread_create(&temper, NULL, tempHandler, NULL)){
      pushAlert("Failed to create thread temperature", "System");
      pushAlert("Moving on", "System");
    }
  }
  if(alarmRun){
    if(pthread_create(&alarmThread, NULL, alarmHandler, NULL)){
      pushAlert("Could not activate alarm", "System");
    }
  }
  system("motion");
  return 0;
}

//Function that gets called to stop all threads.
//It sets the ***Run variable to 2. Meaninge the while loop will stop.
int closeThreads(){
  sqlRun = 2;
  while(sqlRun != 0){
    usleep(100);
  }
  tempRun = 2;
  while(tempRun != 0){
    usleep(100);
  }
  inputRun = 2;
  while(inputRun != 0){
    usleep(100);
  }
  return 0;
}

//Function to create and setup the InterfaceKit, RFIDKit and Camera.
int setupKit(){
  pushAlert("Setting up", "System");

  CPhidgetInterfaceKit_create(&ifKit);
  CPhidget_open((CPhidgetHandle) ifKit, -1);
  printf("Ifkit created\n");
  if(CPhidget_waitForAttachment((CPhidgetHandle) ifKit, SECOND)){
    pushAlert("Kon de volgende kit niet aanmaken: ifKit", "System");
    pushAlert("Shutting down", "System");
    exit(1);
  }
  printf("Ifkit Connected\n");
  printf("Creating RFID\n");
  CPhidgetRFID_create(&rfidKit);
  CPhidgetRFID_set_OnTagLost2_Handler(rfidKit, tagLostHandler, NULL);
  CPhidgetRFID_set_OnTag2_Handler(rfidKit, tagHandler, NULL);
  CPhidget_open((CPhidgetHandle) rfidKit, -1);
  printf("RFIDKit created\n");
  if(CPhidget_waitForAttachment((CPhidgetHandle) rfidKit, SECOND)){
    pushAlert("Kon geen verbinding vinden met rfidKit", "System");
    pushAlert("Verder gaan", "System");
  }
  else{
    printf("RFID connected\n");
    CPhidgetRFID_setAntennaOn(rfidKit, 1);
  }

  pushAlert("Setup complete", "System");
  return 0;
}

//Function to close the kits.
int closeKits(){
  pushAlert("InterfaceKit & RFID Scanner uitzetten", "System");
  CPhidget_close((CPhidgetHandle) ifKit);
  CPhidget_delete((CPhidgetHandle) ifKit);

  CPhidget_close((CPhidgetHandle) rfidKit);
  CPhidget_delete((CPhidgetHandle) rfidKit);
  return 0;
}

//Function to connect to the database.
int connectdb(){
  printf("Connecting to : %s on %s\n", database, server);
  char msg[100];
  //Initialize the connection.
  connection = mysql_init(NULL);

  //Connect to the database.
  //Return 0 if failed. 1 if succeeded.
  if(!mysql_real_connect(connection, server, username, password, database, 0, NULL, 0)){
    printf("Could not connect to %s\n", server);
    printf("Shutting down with error: %s\n", mysql_error(connection));
    exit(1);
  }
  sprintf(msg, "Connectie met %s op %s aangemaakt", database, server);
  pushAlert(msg , "System");
  return 0;
}

//Function to close the connection.
int disconnectdb(){
  pushAlert("Connectie sluiten", "System");

  mysql_close(connection);

  printf("Connection to %s closed\n", server);
  return 0;
}

//Main function. This gets called when the program starts.
int main(int argc, char **argv){
  printf("Starting up\n");
  connectdb(); //Connect to database.
  setupKit(); //Setup ifKit and rfidKit.
  setupThreads(); //Setup all threads.
  getchar(); //Just let the threads run untill the user presses a key.
  closeThreads(); //Close all threads.
  disconnectdb(); //Disconnect from the database.
  closeKits(); //Close all kits.
  return 0; //The end of the program.
}
