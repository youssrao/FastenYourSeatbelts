<?php
session_start();
?>

<!--
begin header.php
-->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>WebApp</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- relatieve link stylesheet -->
        <?php
        //SCRIPT_ROOT is de hoofdmap.
        define('SCRIPT_ROOT', 'https://oege.ie.hva.nl/~vriesmg001');
        echo '<link rel="stylesheet" type="text/css" href="' . SCRIPT_ROOT . '/style.css">';
        ?>
    </head>
    <body>

        <header>
            <!-- Foto Corendon logo -->
            <img   align= "left" src="http://www.meerjazz.nl/uploads/images/sponsoren-onderin/Logo_Corendon_RGB.png" id="CorendonLogo">
            <nav>
                <ul>

                    <?php
                    // Connectie met de database
                    mysql_connect("oege.ie.hva.nl", "vriesmg001", "iMKXdn0rHt/gsn");
                    mysql_select_db("zvriesmg001");

                    // Link naar de Sensoren, Personeel, Home Logout en Signup. Alleen als de gegevens kloppen bij het inloggen.
                    if (isset($_SESSION['ID'])) {
                        echo "<li><a href='" . SCRIPT_ROOT . "/Sensoren.php'>SENSOREN</a></li>";
                        echo "<li><a href='" . SCRIPT_ROOT . "/Personeel.php'>PERSONEEL</a></li>";

                        //Link naar HOME
                        echo "<li><a href='" . SCRIPT_ROOT . "/index.php'>HOME</a></li>";

                        //Als het ID overeenkomt met de gegevens uit de Database komt er een
                        //link naar de LOG OUT en de SIGNUP pagina.


                        echo "<form action='" . SCRIPT_ROOT . "/includes/logout.inc.php'>
                        <button>LOG OUT</button> 
                        </form>";

                        echo "<li><a href='" . SCRIPT_ROOT . "/signup.php'>SIGNUP</a></li>";

                        
                        ?>

                        <script>
                            setInterval(checkheader, 300); //interval voor de connectie met de database.
                            //Refreshed om de 300 ms.

                            //Hier voert het de functie uit met de gegevens van "DatarefreshHeader.php"
                            function checkheader() {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo SCRIPT_ROOT; ?>/DatarefreshHeader.php",
                                    success: function (results) {
                                        document.getElementById('uitroeptekenheader').innerHTML = results;
                                    }
                                });
                            }

                        </script>
                        <span id="uitroeptekenheader"></span>


                        <?php
                    }

                    //Als de gegevens niet kloppen bij het inloggen blijf je op de    
                    //inlogpagina. Hierbij maakt het gebruik van "includes/login.inc.php".
                    else {
                        echo "<form action='includes/login.inc.php' method='POST'>
                            <input type='text' name='uid' placeholder='Username'>
                            <input type='password' name='pwd' placeholder='Password'>
                            <button type='submit'> LOGIN</button>
                            </form>";
                    }
                    ?>


                </ul>
            </nav>

        </header>
        <!--
        Einde header.php
        -->
