<!--
begin NavigationBar.php
-->


<div id="mySidenav" class="sidenav">
    <!-- Knop voor het sluiten van de NavigationBar -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>


    <?php
// Links naar de pagina's van alle sensoren.
   echo '<a href="' . SCRIPT_ROOT . '/sensoren/Licht.php">Licht</a>
         <a href="' . SCRIPT_ROOT . '/sensoren/Deur.php">Deur</a>
         <a href="' . SCRIPT_ROOT . '/sensoren/Thermometer.php">Temperatuur</a>
         <a href="' . SCRIPT_ROOT . '/sensoren/Meldingen.php">Meldingen</a>
         <a href="' . SCRIPT_ROOT . '/sensoren/Camera.php">Camera</a>'
    ?>
</div>

<!-- Knop voor het openenen van de NavigationBar-->
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

<script>
// Wanneer de NavigationBar geopend is, opent het tot 250px breed.
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }
// Hier sluit de NavigationBar tot 0px breed.
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>

<!--
Einde NavigationBar.php
-->
